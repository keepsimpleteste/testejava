# README #

O banco de dados utlizado para o desenvolvimento da aplicação foi o SqlServer rodando em um container docker.

# Container #

* Comando para criação do container

sudo docker run --name mssqlserver -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=@MyMSSQLDocker112' -p 1433:1433 -d microsoft/mssql-server-linux

# Acessos #

* Usuario: root
* Senha: @MyMSSQLDocker112

### Artefatos de banco ###

* Sql de criação do banco: banco.slq
* Sql estrutura do banco: estrutura.slq
* Sql dados do banco: dados.slq

